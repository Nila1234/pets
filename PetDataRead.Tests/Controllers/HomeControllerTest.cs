﻿using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PetDataRead.Controllers;
using PetDataRead.Models;

namespace PetDataRead.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestFemaleOwnerCatName()
        {
            var controller = new HomeController();
            var result = controller.ReadPetData() as ViewResult;
            var catName = (CatName)result.ViewData.Model;
            Assert.IsTrue(catName.FemaleOwnerCatName.Contains("Simba"));            
        }

        [TestMethod]
        public void TestMaleOwnerCatName()
        {
            var controller = new HomeController();
            var result = controller.ReadPetData() as ViewResult;
            var catName = (CatName)result.ViewData.Model;            
            Assert.IsTrue(catName.MaleOwnerCatName.Contains("Tom"));
        }

        [TestMethod]
        public void TestMaleOwnerCatNameSorting()
        {
            var MaleOwnerCatName = new List<string>
                {
                    "Garfield", "Jim", "Max", "Tom"
                };
            var controller = new HomeController();
            var result = controller.ReadPetData() as ViewResult;
            var catName = (CatName)result.ViewData.Model;
            
            CollectionAssert.AreEqual(MaleOwnerCatName, catName.OrderedMaleOwnerCatName);
        }
        [TestMethod]
        public void TestFemaleOwnerCatNameSorting()
        {
            var FemaleOwnerCatName = new List<string>
                {
                    "Garfield", "Simba", "Tabby"
                };
            var controller = new HomeController();
            var result = controller.ReadPetData() as ViewResult;
            var catName = (CatName)result.ViewData.Model;

            CollectionAssert.AreEqual(FemaleOwnerCatName, catName.OrderedFemaleOwnerCatName);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
