﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetDataRead.Common
{
    public class ErrorLog
    {

        public string GetTempPath()
        {
            var path = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
            if (!path.EndsWith("\\")) path += "\\";
            return path;
        }

        public void LogMessageToFile(string msg)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(
                GetTempPath() + "log.txt");
            try
            {
                string logLine = System.String.Format(
                    "{0:G}: {1}.", System.DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }

    }
}