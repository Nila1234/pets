﻿using PetDataRead.Common;
using PetDataRead.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Configuration;


namespace PetDataRead.Controllers
{
    public class HomeController : Controller
    {
        private static ErrorLog errorLog = new ErrorLog();       

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult ReadPetData()
        {
            var model = new CatName
            {
                MaleOwnerCatName = new List<string>(),
                FemaleOwnerCatName = new List<string>()
            };
            try
            {
                return View("CatName", AssignCatName(model));
            }
            catch (Exception e)
            {
                errorLog.LogMessageToFile(e.Message);
                if (string.IsNullOrWhiteSpace(ViewBag.Error))
                {
                    ViewBag.Error = "Please contact administrator.";
                }

                return View("Index");
            }
        }


        private PetOwner[] ProcessPetRec()
        {
            PetOwner[] petOwners;
            var baseUri = WebConfigurationManager.AppSettings["baseUri"];
            var prefix = WebConfigurationManager.AppSettings["prefix"];

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(prefix).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        Task<string> result = content.ReadAsStringAsync();
                        var res = result.Result;
                        if (!string.IsNullOrWhiteSpace(res))
                        {
                            var js = new JavaScriptSerializer();
                            petOwners = js.Deserialize<PetOwner[]>(res);
                        }
                        else
                        {
                            ViewBag.Error = "Null or empty file.";
                            return null;
                        }
                    }
                }
                else
                {
                    ViewBag.Error = "Please contact administrator.";
                    return null;
                }
                return petOwners;
            }
        }

        private CatName AssignCatName(CatName model)
        {

            foreach (var petOwner in ProcessPetRec())
            {
                if (petOwner != null)
                {
                    if (petOwner.Gender == "Male" && petOwner.Pets != null)
                    {
                        foreach (var pet in petOwner.Pets)
                        {
                            if (pet.Type == "Cat")
                                model.MaleOwnerCatName.Add(pet.Name.ToString());
                        }

                    }
                    if (petOwner.Gender == "Female" && petOwner.Pets != null)
                    {
                        foreach (var pet in petOwner.Pets)
                        {
                            if (pet.Type == "Cat")
                                model.FemaleOwnerCatName.Add(pet.Name.ToString());
                        }

                    }
                }

            }

            return model;
        }
    }
}