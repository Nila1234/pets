﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PetDataRead.Models
{
    public class CatName
    {
        public const String Male = "Male";
        public const String Female = "Female";
        public List<string> MaleOwnerCatName { get; set; }
        public List<string> FemaleOwnerCatName { get; set; }

        public List<string> OrderedMaleOwnerCatName
        {
            get => this.MaleOwnerCatName.OrderBy(O => O).ToList();
        }
        public List<string> OrderedFemaleOwnerCatName
        {
            get => this.FemaleOwnerCatName.OrderBy(O => O).ToList();
        }
    }

    
}