﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetDataRead.Models
{
    public class PetOwner
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int age { get; set; }
        public List<Pet> Pets { get; set; }
    }

    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}